# Temporary disable these dts until I figure out how to integrate them easily
# 
# allwinner/overlay/sun50i-h5-ds1307.dtbo
# overlay/sun8i-h3-ds1307.dtbo

SUN50I_OVERLAYS = " \
        allwinner/overlay/sun50i-h5-analog-codec.dtbo \
        allwinner/overlay/sun50i-h5-cir.dtbo \
        allwinner/overlay/sun50i-h5-i2c0.dtbo \
        allwinner/overlay/sun50i-h5-i2c1.dtbo \
        allwinner/overlay/sun50i-h5-i2c2.dtbo \
        allwinner/overlay/sun50i-h5-pps-gpio.dtbo \
        allwinner/overlay/sun50i-h5-pwm.dtbo \
        allwinner/overlay/sun50i-h5-spdif-out.dtbo \
        allwinner/overlay/sun50i-h5-spi-add-cs1.dtbo \
        allwinner/overlay/sun50i-h5-spi-jedec-nor.dtbo \
        allwinner/overlay/sun50i-h5-spi-spidev.dtbo \
        allwinner/overlay/sun50i-h5-uart1.dtbo \
        allwinner/overlay/sun50i-h5-uart2.dtbo \
        allwinner/overlay/sun50i-h5-uart3.dtbo \
        allwinner/overlay/sun50i-h5-usbhost0.dtbo \
        allwinner/overlay/sun50i-h5-usbhost1.dtbo \
        allwinner/overlay/sun50i-h5-usbhost2.dtbo \
        allwinner/overlay/sun50i-h5-usbhost3.dtbo \
        allwinner/overlay/sun50i-h5-w1-gpio.dtbo \
"

SUN8I_OVERLAYS = " \
        overlay/sun8i-h3-analog-codec.dtbo \
        overlay/sun8i-h3-cir.dtbo \
        overlay/sun8i-h3-i2c0.dtbo \
        overlay/sun8i-h3-i2c1.dtbo \
        overlay/sun8i-h3-i2c2.dtbo \
        overlay/sun8i-h3-pps-gpio.dtbo \
        overlay/sun8i-h3-pwm.dtbo \
        overlay/sun8i-h3-spdif-out.dtbo \
        overlay/sun8i-h3-spi-add-cs1.dtbo \
        overlay/sun8i-h3-spi-jedec-nor.dtbo \
        overlay/sun8i-h3-spi-spidev.dtbo \
        overlay/sun8i-h3-uart1.dtbo \
        overlay/sun8i-h3-uart2.dtbo \
        overlay/sun8i-h3-uart3.dtbo \
        overlay/sun8i-h3-usbhost0.dtbo \
        overlay/sun8i-h3-usbhost1.dtbo \
        overlay/sun8i-h3-usbhost2.dtbo \
        overlay/sun8i-h3-usbhost3.dtbo \
        overlay/sun8i-h3-w1-gpio.dtbo \
"
